﻿using Autodesk.Connectivity.Explorer.Extensibility;
using Autodesk.Connectivity.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VDF = Autodesk.DataManagement.Client.Framework;

namespace TrainingTasks
{
    class DownloadFile : IExplorerExtension
    {
        public IEnumerable<CommandSite> CommandSites()
        {
            CommandItem downloadFileCmdItem = new CommandItem("DownloadFileCommand", "Download File")
            {
                NavigationTypes = new SelectionTypeId[] { SelectionTypeId.File, SelectionTypeId.FileVersion},
                ToolbarPaintStyle = PaintStyle.TextAndGlyph,
                MultiSelectEnabled = false
            };

            downloadFileCmdItem.Execute += DownloadFileCommandHandler;

            CommandSite fileContextCmdSite = new CommandSite("DownloadFileCommand.FileContextMenu", "Download Filer")
            {
                Location = CommandSiteLocation.FileContextMenu,
                DeployAsPulldownMenu = false
            };
            fileContextCmdSite.AddCommand(downloadFileCmdItem);

            List<CommandSite> sites = new List<CommandSite>();

            sites.Add(fileContextCmdSite);

            return sites;
        }

        public IEnumerable<CustomEntityHandler> CustomEntityHandlers()
        {
            return null;
        }

        public IEnumerable<DetailPaneTab> DetailTabs()
        {
            return null;
        }

        public IEnumerable<string> HiddenCommands()
        {
            return null;
        }

        public void OnLogOff(IApplication application)
        {         
        }

        public void OnLogOn(IApplication application)
        {
        }

        public void OnShutdown(IApplication application)
        {
        }

        public void OnStartup(IApplication application)
        {
        }

        public static void DownloadFileFrom(File selectedFile, string path, VDF.Vault.Currency.Connections.Connection connection)
        {
            VDF.Vault.Settings.AcquireFilesSettings settings = new VDF.Vault.Settings.AcquireFilesSettings(connection);
            VDF.Vault.Currency.Entities.FileIteration fileToDownload = new VDF.Vault.Currency.Entities.FileIteration(connection, selectedFile);

            settings.AddEntityToAcquire(fileToDownload);
            settings.DefaultAcquisitionOption = VDF.Vault.Settings.AcquireFilesSettings.AcquisitionOption.Download;
            settings.LocalPath = new VDF.Currency.FolderPathAbsolute(path);

            connection.FileManager.AcquireFiles(settings);
        }
        
        void DownloadFileCommandHandler(object s, CommandItemEventArgs e)
        {
            try
            {  
                VDF.Vault.Currency.Connections.Connection connection = e.Context.Application.Connection;

                ISelection selection = e.Context.CurrentSelectionSet.First();

                File selectedFile = null;

                if (selection.TypeId == SelectionTypeId.File)
                {
                    selectedFile = connection.WebServiceManager.DocumentService.GetLatestFileByMasterId(selection.Id);
                }
                else if (selection.TypeId == SelectionTypeId.FileVersion)
                {
                    selectedFile = connection.WebServiceManager.DocumentService.GetFileById(selection.Id);
                }

                DownloadFileFrom(selectedFile, SetLocalStorageFolder.SetPath, connection);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
