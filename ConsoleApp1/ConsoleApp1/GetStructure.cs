﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Linq;

using Autodesk.Connectivity.Explorer.Extensibility;
using Autodesk.Connectivity.WebServices;
using VDF = Autodesk.DataManagement.Client.Framework;


namespace TrainingTasks
{  
    internal class GetStructure : IExplorerExtension
    {
        public IEnumerable<CommandSite> CommandSites()
        {
            CommandItem structureCmdItem = new CommandItem("StructureCommand", "GetStructire...")
            {
                ToolbarPaintStyle = PaintStyle.TextAndGlyph,
                MultiSelectEnabled = false
            };

            structureCmdItem.Execute += GetStructureCommandHandler;

            CommandSite toolbarCmdSite = new CommandSite("StructureCommand.Toolbar", "Get Structire")
            {
                Location = CommandSiteLocation.AdvancedToolbar,
                DeployAsPulldownMenu = false
            };
            toolbarCmdSite.AddCommand(structureCmdItem);

            List<CommandSite> sites = new List<CommandSite>();
            sites.Add(toolbarCmdSite);

            return sites;
        }

        public IEnumerable<CustomEntityHandler> CustomEntityHandlers()
        {
            return null;
        }

        public IEnumerable<DetailPaneTab> DetailTabs()
        {
            return null;
        }

        public IEnumerable<string> HiddenCommands()
        {
            return null;
        }

        public void OnLogOff(IApplication application)
        {
        }

        public void OnLogOn(IApplication application)
        {
        }

        public void OnShutdown(IApplication application)
        {
        }

        public void OnStartup(IApplication application)
        {
        }

        private XElement GetSubdirectoryXML(Folder dir, VDF.Vault.Currency.Connections.Connection connection)
        {
            var xmlInfo = new XElement("Directory", new XAttribute("name", dir.Name));
            File[] files = connection.WebServiceManager.DocumentService.GetLatestFilesByFolderId(dir.Id, false);
            if (files != null)
            {
                foreach (var file in files)
                {
                    xmlInfo.Add(new XElement("File", file.Name ));
                }
            }           
            Folder[] folders = connection.WebServiceManager.DocumentService.GetFoldersByParentId(dir.Id, false);
            if (folders != null)
            {
                foreach (var subDir in folders)
                {
                    xmlInfo.Add(GetSubdirectoryXML(subDir, connection));
                }
            }
            return xmlInfo;
        }

        void GetStructureCommandHandler(object s, CommandItemEventArgs e)
        {
            try
            {
                VDF.Vault.Currency.Connections.Connection connection = e.Context.Application.Connection;

                Folder rootFolder = connection.WebServiceManager.DocumentService.GetFolderRoot();

                XDocument xdoc = new XDocument();
                xdoc.Add(GetSubdirectoryXML(rootFolder, connection));
                xdoc.Save(SetLocalStorageFolder.SetPath + @"\Vault.xml");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
