﻿using Autodesk.Connectivity.Explorer.Extensibility;
using Autodesk.Connectivity.WebServices;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Windows.Forms;
using VDF = Autodesk.DataManagement.Client.Framework;

namespace TrainingTasks
{
    class DownloadFolder : IExplorerExtension
    {
        public IEnumerable<CommandSite> CommandSites()
        {
            CommandItem downloadFolderCmdItem = new CommandItem("DownloadFolderCommand", "Download Folder")
            {
                NavigationTypes = new SelectionTypeId[] { SelectionTypeId.Folder },
                ToolbarPaintStyle = PaintStyle.TextAndGlyph,
                MultiSelectEnabled = false
            };

            downloadFolderCmdItem.Execute += DownloadFolderCommandHandler;

            CommandSite folderContextCmdSite = new CommandSite("DownloadFolderCommand.FolderContextMenu", "Download Folder")
            {
                Location = CommandSiteLocation.FolderContextMenu,
                DeployAsPulldownMenu = false
            };
            folderContextCmdSite.AddCommand(downloadFolderCmdItem);

            List<CommandSite> sites = new List<CommandSite>();

            sites.Add(folderContextCmdSite);

            return sites;
        }

        public IEnumerable<CustomEntityHandler> CustomEntityHandlers()
        {
            return null;
        }

        public IEnumerable<DetailPaneTab> DetailTabs()
        {
            return null;
        }

        public IEnumerable<string> HiddenCommands()
        {
            return null;
        }

        public void OnLogOff(IApplication application)
        {         
        }

        public void OnLogOn(IApplication application)
        {
        }

        public void OnShutdown(IApplication application)
        {
        }

        public void OnStartup(IApplication application)
        {
        }

        private void GetZipFromFolder(Folder folder, VDF.Vault.Currency.Connections.Connection conn)
        {
            string directoryPath = @"C:\ProgramData\" + folder.Name;
            string zipPath = SetLocalStorageFolder.SetPath + @"\" + folder.Name + ".zip";
            
            File[] files = conn.WebServiceManager.DocumentService.GetLatestFilesByFolderId(folder.Id, false);

            if (files != null && files.Length > 0)
            {
                System.IO.Directory.CreateDirectory(directoryPath);          
                foreach (File file in files)
                {
                    DownloadFile.DownloadFileFrom(file, directoryPath, conn);
                }
                if (System.IO.File.Exists(zipPath))
                {
                    System.IO.File.Delete(zipPath);
                }
                ZipFile.CreateFromDirectory(directoryPath, zipPath);
                System.IO.Directory.Delete(directoryPath, true);
            }
        }

        void DownloadFolderCommandHandler(object s, CommandItemEventArgs e)
        {
            try
            {              
                VDF.Vault.Currency.Connections.Connection connection = e.Context.Application.Connection;

                ISelection selection = e.Context.CurrentSelectionSet.First();

                Folder selectedFolder = connection.WebServiceManager.DocumentService.GetFolderById(selection.Id);

                GetZipFromFolder(selectedFolder, connection);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
