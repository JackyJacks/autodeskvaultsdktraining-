﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

using Autodesk.Connectivity.Explorer.Extensibility;


[assembly: AssemblyCompany("I")]
[assembly: AssemblyProduct("SetLocalStorageFolder")]
[assembly: AssemblyDescription("Testing App")]

[assembly: Autodesk.Connectivity.Extensibility.Framework.ExtensionId("3619482D-F0C2-4688-94A6-585E567ADBEB")]

[assembly: Autodesk.Connectivity.Extensibility.Framework.ApiVersion("11.0")]

namespace TrainingTasks
{  
    class SetLocalStorageFolder : IExplorerExtension
    {
        public static string SetPath { get; set; }
        public IEnumerable<CommandSite> CommandSites()
        {
            CommandItem LocalCmdItem = new CommandItem("LocalStorageFolderCommand", "Set Local Sto...")
            {
                ToolbarPaintStyle = PaintStyle.TextAndGlyph,
                MultiSelectEnabled = false
            };

            LocalCmdItem.Execute += LocalStorageFolderCommandHandler;

            CommandSite toolbarCmdSite = new CommandSite("LocalStorageFolderCommand.Toolbar", "Set Local Storage Folder")
            {
                Location = CommandSiteLocation.AdvancedToolbar,
                DeployAsPulldownMenu = false
            };
            toolbarCmdSite.AddCommand(LocalCmdItem);

            List<CommandSite> sites = new List<CommandSite>();
            sites.Add(toolbarCmdSite);

            return sites;
        }

        public IEnumerable<CustomEntityHandler> CustomEntityHandlers()
        {
            return null;
        }

        public IEnumerable<DetailPaneTab> DetailTabs()
        {
            return null;
        }

        public IEnumerable<string> HiddenCommands()
        {
            return null;
        }

        public void OnLogOff(IApplication application)
        {
        }

        public void OnLogOn(IApplication application)
        {
        }

        public void OnShutdown(IApplication application)
        {
        }

        public void OnStartup(IApplication application)
        {
        }


        void LocalStorageFolderCommandHandler(object s, CommandItemEventArgs e)
        {
            try
            {
                SetLocalStrForm form = new SetLocalStrForm();
                form.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: {0}", ex.Message);
            }
        }
    }
}
