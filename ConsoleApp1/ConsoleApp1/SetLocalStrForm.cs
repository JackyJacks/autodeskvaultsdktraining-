﻿using System.Windows.Forms;


namespace TrainingTasks
{
    public partial class SetLocalStrForm : Form
    {
        Label label;
        private TextBox directoryPath;
        private Button selectPathButton;
        private Button okButton;

        public string Path
        {
            get { return directoryPath.Text; }
        }

        public void SetDirectoryPath(string text)
        {
            this.directoryPath.Text = text;
        }

        public SetLocalStrForm()
        {
            InitializeComponent();
        }

        private void Selection()
        {
            using (var folderBrowserDialog = new FolderBrowserDialog())
            {
                DialogResult result = folderBrowserDialog.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                {
                    string filePath = folderBrowserDialog.SelectedPath;
                    this.SetDirectoryPath(filePath);
                    SetLocalStorageFolder.SetPath = filePath;               
                }
            }
        }

        //[STAThread]
        //static void Main(string[] args)
        //{
        //    SetLocalStrForm form = new SetLocalStrForm();
            
        //    form.ShowDialog();
        //}
    }
}
