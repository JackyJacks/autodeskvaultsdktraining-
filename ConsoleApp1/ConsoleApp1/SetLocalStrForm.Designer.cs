﻿namespace TrainingTasks
{
    partial class SetLocalStrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.selectPathButton = new System.Windows.Forms.Button();
            this.directoryPath = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(192, 65);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(88, 24);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.Click += new System.EventHandler(this.okButtonClick);
            // 
            // selectPathButton
            // 
            this.selectPathButton.Location = new System.Drawing.Point(255, 8);
            this.selectPathButton.Name = "selectPathButton";
            this.selectPathButton.Size = new System.Drawing.Size(22, 22);
            this.selectPathButton.TabIndex = 2;
            this.selectPathButton.Text = ". . .";
            this.selectPathButton.Click += new System.EventHandler(this.selectPathButtonClick);
            // 
            // directoryPath
            // 
            this.directoryPath.Location = new System.Drawing.Point(88, 8);
            this.directoryPath.Name = "directoryPath";
            this.directoryPath.ReadOnly = true;
            this.directoryPath.Size = new System.Drawing.Size(160, 22);
            this.directoryPath.TabIndex = 3;
            if ( string.IsNullOrEmpty(this.directoryPath.Text))
            {
                this.directoryPath.Text = SetLocalStorageFolder.SetPath;
            }
            // 
            // label
            // 
            this.label.Location = new System.Drawing.Point(8, 8);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(72, 16);
            this.label.TabIndex = 4;
            this.label.Text = "Path";
            // 
            // SetLocalStrForm
            // 
            this.AcceptButton = this.okButton;
            this.ClientSize = new System.Drawing.Size(292, 101);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.selectPathButton);
            this.Controls.Add(this.directoryPath);
            this.Controls.Add(this.label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SetLocalStrForm";
            this.Text = "Set Local Storage Folder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void okButtonClick(object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void selectPathButtonClick(object sender, System.EventArgs e)
        {
            Selection();
        }
    }    
}